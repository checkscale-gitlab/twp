# -*- coding: utf-8 -*-
##############################################################################
#    Teeworlds Web Panel
#    Copyright (C) 2016-2017  Alexandre Díaz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################
from twp import twp, BANLIST, flash_errors, get_session_user, get_session_server_permission_level
import time
from datetime import datetime, timedelta
from mergedict import ConfigDict
from flask import request, session, redirect, url_for, abort, render_template, \
    flash, jsonify, send_file, current_app
from flask_babel import Babel, _, format_datetime
from sqlalchemy import or_, func, desc
from tools.banner_generator import BannerGenerator
from twpl.models import AppWebConfig, ServerInstance, Player, ServerStaffRegistry, User, \
    PlayerServerInstance, db_add_and_commit, db
from twpl.base import get_linux_distribution, get_server_net_info, get_local_servers, HTMLColorToRGBA, \
    get_tw_masterserver_list, PUBLIC_IP
from tools.netstat import netstat
from werkzeug import check_password_hash, generate_password_hash


#################################
# TOOLS
#################################
def get_login_tries():
    return int(session.get('login_try')) if 'login_try' in session else 0


#################################
# GET
#################################
@twp.route("/restore_admin", methods=['GET', 'POST'])
def restore_admin():
    if not BANLIST.find(request.remote_addr) and get_login_tries() >= current_app.config['LOGIN_MAX_TRIES']:
        BANLIST.add(request.remote_addr, current_app.config['LOGIN_BAN_TIME'])
        session['login_try'] = 0
        abort(403)

    from twpl.forms import ResetAdminPasswordForm
    reset_admin_password_form = ResetAdminPasswordForm()

    if request.method == 'POST':
        request_skey = request.form['skey']
        request_username = request.form['username']
        request_passwd = request.form['userpass']

        if reset_admin_password_form.validate_on_submit():
            request_skey = request.form['skey']
            if current_app.config['SECRET_KEY'] == request_skey:
                from twp import SUPERUSER_ID
                admin_user = User.query.get(SUPERUSER_ID)
                if admin_user.username == request_username:
                    admin_user.passwd_hash = generate_password_hash(request_passwd)
                    db_add_and_commit(admin_user)
                    session['login_try'] = 0
                    flash(_('Admin Restored Successfully ;)'), 'success')
                    return redirect(url_for('twp.login'))
                else:
                    session['login_try'] = get_login_tries() + 1
                    session['last_login_try'] = int(time.time())
                    flash(_('Invalid Admin User Name!'), 'danger')
                    return redirect(url_for('twp.overview'))
            else:
                session['login_try'] = get_login_tries() + 1
                session['last_login_try'] = int(time.time())
                flash(_('Invalid Secret!'), 'danger')
                return redirect(url_for('twp.overview'))
        else:
            flash_errors(reset_admin_password_form)
            return redirect(url_for('twp.restore_admin'))
    else:
        return render_template('pages/restore_admin.html', rap_form=reset_admin_password_form)


@twp.route("/install", methods=['GET'])
def installation():
    app_config = AppWebConfig.query.get(1)
    if app_config.installed:
        abort(404)
    return render_template('pages/install.html', appconfig=app_config)


@twp.route("/", methods=['GET'])
def overview():
    session['prev_url'] = request.path
    return render_template('pages/index.html', dist=get_linux_distribution(), ip=PUBLIC_IP)


@twp.route('/search', methods=['GET'])
def search():
    searchword = request.args.get('r', '')

    sk = "%%%s%%" % searchword
    servers = ServerInstance.query.filter(or_(ServerInstance.name.like(sk),
                                              ServerInstance.base_folder.like(sk))).all()
    players = Player.query.filter(Player.name.like(sk)).all()
    return render_template('pages/search.html', search=searchword, servers=servers, players=players)


@twp.route('/players', methods=['GET'])
def players():
    session['prev_url'] = request.path

    players = Player.query.order_by(desc(Player.last_seen_date)).order_by(desc(Player.name)).all()
    return render_template('pages/players.html', players=players)


@twp.route('/maps', methods=['GET'])
def maps():
    session['prev_url'] = request.path
    return redirect(url_for('twp.overview'))


@twp.route('/servers', methods=['GET'])
def servers():
    session['prev_url'] = request.path

    ServerInstance.query.update({ServerInstance.status: 0})
    db.session.commit()

    from twpl.forms import InstallModForm
    install_mod_form = InstallModForm()

    netstat_obj = netstat()
    for conn in netstat_obj:
        if not conn[2]:
            continue
        (rest, base_folder, bin) = conn[2].rsplit('/', 2)
        srv = ServerInstance.query.filter(ServerInstance.port.ilike(conn[0]),
                                          ServerInstance.base_folder.ilike(base_folder),
                                          ServerInstance.bin.ilike(bin))
        if srv.count() > 0:
            srv = srv.one()
            net_server_info = get_server_net_info("127.0.0.1", [srv])[0]
            # FIXME: Check info integrity
            if net_server_info and net_server_info['netinfo'].gametype:
                srv.status = 1
                srv.name = net_server_info['netinfo'].name
                srv.gametype = net_server_info['netinfo'].gametype
                db_add_and_commit(srv)

    flash_errors(install_mod_form)
    return render_template('pages/servers.html',
                           servers=get_local_servers(current_app.config['SERVERS_BASEPATH']),
                           install_mod_form=install_mod_form)


@twp.route('/server/<int:srvid>', methods=['GET'])
def server(srvid):
    session['prev_url'] = request.path
    srv = ServerInstance.query.get(srvid)

    netinfo = None
    if srv:
        netinfo = get_server_net_info("127.0.0.1", [srv])[0]['netinfo']
    else:
        flash(_('Server not found!'), "danger")

    users_reg = ServerStaffRegistry.query.filter(ServerStaffRegistry.server_id == srvid).order_by(desc(ServerStaffRegistry.date)).all()
    return render_template('pages/server.html', ip=PUBLIC_IP, server=srv, netinfo=netinfo,
                           uidperms=get_session_server_permission_level(srv.id),
                           users_reg=users_reg)


@twp.route('/server/<int:srvid>/banner', methods=['GET'])
def generate_server_banner(srvid):
    srv = ServerInstance.query.get(srvid)
    if srv:
        netinfo = get_server_net_info("127.0.0.1", [srv])[0]['netinfo']
        banner_image = BannerGenerator((600, 40), srv, netinfo)

        if 'title' in request.values:
            banner_image.titleColor = HTMLColorToRGBA(request.values.get('title'))
        if 'detail' in request.values:
            banner_image.detailColor = HTMLColorToRGBA(request.values.get('detail'))
        if 'address' in request.values:
            banner_image.addressColor = HTMLColorToRGBA(request.values.get('address'))
        if 'grads' in request.values:
            banner_image.gradStartColor = HTMLColorToRGBA(request.values.get('grads'))
        if 'grade' in request.values:
            banner_image.gradEndColor = HTMLColorToRGBA(request.values.get('grade'))

        return send_file(banner_image.generate(PUBLIC_IP), attachment_filename='twbanner.png', as_attachment=False)


@twp.route('/login', methods=['GET', 'POST'])
def login():
    if not BANLIST.find(request.remote_addr) and get_login_tries() >= current_app.config['LOGIN_MAX_TRIES']:
        BANLIST.add(request.remote_addr, current_app.config['LOGIN_BAN_TIME'])
        session['login_try'] = 0
        abort(403)

    from twpl.forms import LoginForm
    login_form = LoginForm()

    if request.method == 'POST':
        if login_form.validate_on_submit():
            request_username = request.form['username']
            request_passwd = request.form['password']
            current_url = session['prev_url'] if 'prev_url' in session else url_for('twp.overview')

            dbuser = User.query.filter(User.username == request_username)
            if dbuser.count() > 0:
                dbuser = dbuser.one()
                if check_password_hash(dbuser.passwd_hash, request_passwd):
                    session['logged_in'] = True
                    session['uid'] = dbuser.id
                    session['last_activity'] = int(time.time())
                    session['username'] = dbuser.username
                    session['demo'] = dbuser.is_demo
                    session['timezone'] = dbuser.timezone
                    flash(_('You are logged in!'), 'success')

                    dbuser.last_login_date = func.now()
                    db_add_and_commit(dbuser)

                    if current_url == url_for('twp.login'):
                        return redirect(url_for('twp.overview'))
                    return redirect(current_url)

            session['login_try'] = get_login_tries() + 1
            session['last_login_try'] = int(time.time())
            flash(_('Invalid username or password! ({0}/{1})').format(get_login_tries(),
                                                                      current_app.config['LOGIN_MAX_TRIES']), 'danger')
    flash_errors(login_form)
    return render_template('pages/login.html', login_form=login_form)


@twp.route('/logout', methods=['GET'])
def logout():
    #     current_url = session['prev_url'] if 'prev_url' in session else url_for('twp.overview')
    session.pop('logged_in', None)
    session.pop('last_activity', None)
    session.pop('name', None)
    session.pop('prev_url', None)
    session.pop('login_try', None)
    session.pop('last_login_try', None)
    session.pop('uid', None)
    session.pop('timezone', None)
    session.pop('demo', None)
    flash(_('You are logged out!'), 'success')
    return redirect(url_for('twp.overview'))


@twp.route('/user_reg/<string:token>', methods=['GET', 'POST'])
def user_reg(token):
    user = User.query.filter(User.token == token)
    if user.count() < 1:
        abort(403)
    user = user.one()
    from twpl.forms import UserRegistrationForm
    user_reg_form = UserRegistrationForm()

    if request.method == 'POST':
        if not user_reg_form.validate_on_submit():
            flash_errors(user_reg_form)
            return render_template('pages/user_register.html', user=user, reg_form=user_reg_form, last_page=True)
        user_count = User.query.filter(User.username.ilike(request.form['username'])).count()
        if user_count > 0:
            flash(_('Username already in use!'), 'danger')
            return render_template('pages/user_register.html', user=user, reg_form=user_reg_form, last_page=True)
        user.token = None
        user.passwd_hash = generate_password_hash(request.form['userpass'])
        user.username = request.form['username']
        db_add_and_commit(user)
        return redirect(url_for('twp.login'))
    return render_template('pages/user_register.html', user=user, reg_form=user_reg_form)


#################################
# POST
#################################
@twp.route('/_finish_installation', methods=['POST'])
def finish_installation():
    app_config = AppWebConfig.query.get(1)
    if app_config.installed:
        abort(404)

    adminuser = request.form.get('adminuser', False)
    adminpass = request.form.get('adminpass', False)
    if not adminuser or not adminpass or User.query.count() != 0:
        return jsonify({'error': True, 'errormsg': _('Missed params!')})

    app_config.brand = request.form['brand'] if 'brand' in request.form else ''
    if 'brand-url' in request.form.keys():
        app_config.brand_url = request.form['brand-url']
    app_config.installed = True
    db_add_and_commit(app_config)
    admin_user = User(
        username=adminuser,
        passwd_hash=generate_password_hash(adminpass)
    )
    db_add_and_commit(admin_user)
    return jsonify({'success': True})


@twp.route('/_refresh_host_localtime', methods=['POST'])
def refresh_host_localtime():
    dt = datetime.utcnow()
#     return jsonify(twpl.host_localtime())
    return jsonify({'localtime': format_datetime(dt, 'short'), 'localzone': format_datetime(dt, "z")})


@twp.route('/_get_all_online_servers', methods=['POST'])
def get_all_online_servers():
    return jsonify(get_tw_masterserver_list(PUBLIC_IP))


@twp.route('/_get_chart_values/<string:chart>', methods=['POST'])
@twp.route('/_get_chart_values/<string:chart>/<int:srvid>', methods=['POST'])
def get_chart_values(chart, srvid=None):
    today = datetime.utcnow()
    startday = (today - timedelta(days=6)).replace(hour=0, minute=0, second=0)
    starthour = (today - timedelta(hours=24)).replace(second=0)
    allowed_dates = [(startday + timedelta(days=i)).strftime("%Y-%m-%d") for i in range(0, 7)]
    allowed_hours = [(starthour + timedelta(minutes=i)).strftime("%Y-%m-%d %H:%M") for i in xrange(0, 24 * 60)]
    labels = dict()
    values = dict()

    if chart.lower() == 'server':
        # 7 Days
        values['players7d'] = list()
        players = PlayerServerInstance.query.filter(PlayerServerInstance.server_id == srvid,
                                                    PlayerServerInstance.date >= startday).all()
        if not players:
            return jsonify({'error': True, 'errormsg': _('Invalid Operation: Server not found!')})
        chart_data = ConfigDict()
        for dbplayer in players:
            strdate = dbplayer.date.strftime("%Y-%m-%d")
            if strdate not in chart_data.keys() or dbplayer.name not in chart_data[strdate]:
                chart_data.merge({strdate: [dbplayer.name]})
        for secc in allowed_dates:
            values['players7d'].append({'x': secc, 'y': len(chart_data[secc]) if secc in chart_data else 0})
        # 24 Hours
        values['players24h'] = list()
        players = PlayerServerInstance.query.filter(PlayerServerInstance.server_id == srvid,
                                                    PlayerServerInstance.date >= starthour).all()
        if not players:
            return jsonify({'error': True, 'errormsg': _('Invalid Operation: Server not found!')})
        chart_data = ConfigDict()
        for dbplayer in players:
            strdate = dbplayer.date.strftime("%Y-%m-%d %H:%M")
            if strdate not in chart_data.keys() or dbplayer.name not in chart_data[strdate]:
                chart_data.merge({strdate: [dbplayer.name]})
        for secc in allowed_hours:
            values['players24h'].append({'x': secc, 'y': len(chart_data[secc]) if secc in chart_data else 0})

        # Top Clans
        query_data = db.session.execute("SELECT count(clan) as num, clan FROM \
                                        (SELECT DISTINCT name,clan,server_id FROM player_server_instance \
                                        WHERE clan IS NOT NULL AND clan <> '') as tbl WHERE tbl.server_id=:id GROUP BY clan \
                                        ORDER BY num DESC LIMIT 5", {"id": srvid})
        if query_data:
            labels['topclan'] = list()
            values['topclan'] = list()
            for value in query_data:
                if not value.clan < 1:
                    labels['topclan'].append("%s [%s]" % (value.clan, value.num))
                    values['topclan'].append(value.num)

        # Top Countries
        query_data = db.session.execute("SELECT count(country) as num, country FROM \
                                        (SELECT DISTINCT name,country,server_id FROM player_server_instance \
                                        WHERE country IS NOT NULL AND country <> '') as tbl WHERE tbl.server_id=:id GROUP BY country \
                                        ORDER BY num DESC LIMIT 5", {"id": srvid})
        if query_data:
            labels['topcountry'] = list()
            values['topcountry'] = list()
            for value in query_data:
                if not value.country < 1:
                    labels['topcountry'].append(value.country)
                    values['topcountry'].append(value.num)

        return jsonify({'success': True, 'series': values, 'labels': labels})
    elif chart.lower() == 'machine':
        # Mods 7 Days
        values['mods7d'] = list()
        labels['mods7d'] = list()
        players = PlayerServerInstance.query.filter(PlayerServerInstance.date >= startday).all()
        if not players:
            return jsonify({'error': True, 'errormsg': _('Invalid Operation: Server not found!')})
        chart_data = ConfigDict()
        for dbplayer in players:
            srv = ServerInstance.query.get(dbplayer.server_id)
            if srv:
                if srv.base_folder not in chart_data.keys():
                    chart_data.merge({srv.base_folder: {srv.id: [dbplayer.name]}})
                elif srv.id not in chart_data[srv.base_folder].keys():
                    chart_data[srv.base_folder].update({srv.id: [dbplayer.name]})
                elif dbplayer.name not in chart_data[srv.base_folder][srv.id]:
                    chart_data[srv.base_folder][srv.id].append(dbplayer.name)
        servers = ServerInstance.query
        ## Cambiar esto por la obtencion de la lista de mods
        for mod in chart_data.keys():
            labels['mods7d'].append(mod)
            for idx, server in enumerate(servers):
                if len(values['mods7d']) - 1 < idx:
                    values['mods7d'].append([])
                values['mods7d'][idx].append(len(chart_data[mod][server.id]) if server.id in chart_data[mod].keys() else 0)
        # 7 Days
        values['players7d'] = list()
        players = PlayerServerInstance.query.filter(PlayerServerInstance.date >= startday).all()
        if not players:
            return jsonify({'error': True, 'errormsg': _('Invalid Operation: Server not found!')})
        chart_data = ConfigDict()
        for dbplayer in players:
            strdate = dbplayer.date.strftime("%Y-%m-%d")
            if strdate not in chart_data.keys() or dbplayer.name not in chart_data[strdate]:
                chart_data.merge({strdate: [dbplayer.name]})
        for secc in allowed_dates:
            values['players7d'].append({'x': secc, 'y': len(chart_data[secc]) if secc in chart_data else 0})
        # 24 Hours
        values['players24h'] = list()
        players = PlayerServerInstance.query.filter(PlayerServerInstance.date >= starthour).all()
        if not players:
            return jsonify({'error': True, 'errormsg': _('Invalid Operation: Server not found!')})
        chart_data = ConfigDict()
        for dbplayer in players:
            strdate = dbplayer.date.strftime("%Y-%m-%d %H:%M")
            if strdate not in chart_data.keys() or dbplayer.name not in chart_data[strdate]:
                chart_data.merge({strdate: [dbplayer.name]})
        for secc in allowed_hours:
            values['players24h'].append({'x': secc, 'y': len(chart_data[secc]) if secc in chart_data else 0})
        return jsonify({'success': True, 'series': values, 'labels': labels})
    return jsonify({'error': True, 'errormsg': _('Undefined Chart!')})


@twp.route('/_get_server_instances_online', methods=['POST'])
def get_server_instances_online():
    servers = ServerInstance.query.filter(ServerInstance.status == 1)
    return jsonify({'success': True, 'num': servers.count()})


@twp.route('/_set_timezone', methods=['POST'])
def set_timezone():
    tzstr = request.form['tzstr'] if 'tzstr' in request.form else None
    if not tzstr:
        return jsonify({'error': True, 'errormsg': _('Invalid TimeZone!')})

    session['timezone'] = tzstr
    sess_user = get_session_user()
    if sess_user:
        sess_user.timezone = tzstr
        db_add_and_commit(sess_user)
    return jsonify({'success': True})
