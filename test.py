# -*- coding: utf-8 -*-
##############################################################################
#    Teeworlds Web Panel
#    Copyright (C) 2016-2017  Alexandre Díaz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################
# UNIT TEST

import os
from twp import create_app, BANLIST
from twpl.models import db_init
from twpl.configs import TWPConfigTest
import unittest
import tempfile
import json


class TWPTestCase(unittest.TestCase):

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def setUp(self):
        self.db_fd, self.dbfile = tempfile.mkstemp()
        self.twp_app = create_app(TWPConfigTest())
        self.twp_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///%s' % self.dbfile
        self.twp_app.config['SERVERS_BASEPATH'] = tempfile.mkdtemp()
        self.test_server_folder = os.path.join(self.twp_app.config['SERVERS_BASEPATH'], 'twsrv')
        self.app = self.twp_app.test_client()
        db_init(self.twp_app)
        self.app.post('/_finish_installation', data=dict(
            adminuser='admin',
            adminpass='admin',
            brand='Testing',
        ), follow_redirects=True)

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.dbfile)

    def test_login_logout(self):
        rv = self.login('admin', 'admin')
        assert 'You are logged in!' in rv.data
        self.logout()
        rv = self.login('1234', '1234')
        assert 'Invalid username or password!' in rv.data

    def test_servers(self):
        rv = self.app.get('/servers', follow_redirects=True)
        assert 'No servers found!' in rv.data
        os.mkdir(self.test_server_folder)

        # + CREATE SERVER INSTANCE
        # NO AUTH
        rv = self.app.post('/_create_server_instance/twsrv', data=dict(
            fileconfig='testsrv'
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_create_server_instance/twsrv', data=dict(
            fileconfig='testsrv'
        ), follow_redirects=True)
        assert 'success' in rv.data
        self.logout()

        # + SET INSTANCE BINARY
        # NO AUTH
        rv = self.app.post('/_set_server_binary/1/teeworlds_srv', data=dict(
            fileconfig='testsrv'
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_set_server_binary/1/teeworlds_srv', data=dict(
            fileconfig='testsrv'
        ), follow_redirects=True)
        assert 'invalidBinary' in rv.data
        with open(os.path.join(self.test_server_folder, 'teeworlds_srv'), 'wb') as f:
            f.write(b"\x01\x02\x03")
        rv = self.app.post('/_set_server_binary/1/teeworlds_srv', data=dict(
            fileconfig='testsrv'
        ), follow_redirects=True)
        assert 'success' in rv.data
        self.logout()

        # + SAVE SERVER CONFIG
        # NO AUTH
        rv = self.app.post('/_save_server_config/1', data=dict(
            alsrv=0,
            srvcfg=""
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_save_server_config/1', data=dict(
            alsrv=0,
            srvcfg='sv_name test\nsv_port 8305'
        ), follow_redirects=True)
        assert 'success' in rv.data
        # - Special Characters
        self.login('admin', 'admin')
        rv = self.app.post('/_save_server_config/1', data=dict(
            alsrv=0,
            srvcfg='sv_name test\nsv_port 8305\nsv_motd éáçñèö'
        ), follow_redirects=True)
        assert 'success' in rv.data
        self.logout()

        # + GET SERVER CONFIG
        # NO AUTH
        rv = self.app.post('/_get_server_config/1', follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_get_server_config/1', follow_redirects=True)
        assert 'sv_name test' in rv.data
        self.logout()

        # + START SERVER
        # NO AUTH
        rv = self.app.post('/_start_server_instance/1', follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        # TODO

        # STOP SERVER
        # NO AUTH
        rv = self.app.post('/_stop_server_instance/1', follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        # TODO

        # + DELETE SERVER INSTANCE
        # NO AUTH
        rv = self.app.post('/_remove_server_instance/1/1', data=dict(
            fileconfig='testsrv'
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_remove_server_instance/1/1', data=dict(
            fileconfig='testsrv'
        ), follow_redirects=True)
        assert 'success' in rv.data
        self.logout()

    def test_players(self):
        rv = self.app.get('/players', follow_redirects=True)
        assert 'No players found!' in rv.data

    def test_settings(self):
        # + CREATE USER SLOT
        # NO AUTH
        rv = self.app.post('/_create_user_slot', data=None, follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_create_user_slot', data=None, follow_redirects=True)
        assert 'success' in rv.data
        self.logout()
        json_data = json.loads(rv.data)
        new_user_id = json_data['user']['id']
        new_user_token = json_data['user']['token']
        del json_data
        # + CHANGE USER TYPE
        # NO AUTH
        rv = self.app.post('/_change_user_type', data=dict(
            uid=new_user_id,
            isdemo=1
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_change_user_type', data=dict(
            uid=new_user_id,
            isdemo=1
        ), follow_redirects=True)
        assert 'success' in rv.data and json.loads(rv.data)['isdemo']
        self.logout()
        # + REMOVE USER
        # NO AUTH
        rv = self.app.post('/_remove_user/%d' % new_user_id, data=None, follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_remove_user/%d' % new_user_id, data=None, follow_redirects=True)
        assert 'success' in rv.data
        self.logout()
        del new_user_id
        del new_user_token
        # + CREATE USER PERM
        # NO AUTH
        rv = self.app.post('/_create_permission_level', data=dict(
            name='Perm Test',
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_create_permission_level', data=dict(
            name='Perm Test',
        ), follow_redirects=True)
        assert 'success' in rv.data
        self.logout()
        new_perm_id = json.loads(rv.data)['perm']['id']
        # + MODIFY USER PERM
        # NO AUTH
        rv = self.app.post('/_change_permission_level', data=dict(
            id=new_perm_id,
            perm='config',
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_change_permission_level', data=dict(
            id=new_perm_id,
            perm='config',
        ), follow_redirects=True)
        assert 'success' in rv.data
        self.logout()
        # + REMOVE USER PERM
        # NO AUTH
        rv = self.app.post('/_remove_permission_level/%d' % new_perm_id, data=None, follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        rv = self.app.post('/_remove_permission_level/%d' % new_perm_id, data=None, follow_redirects=True)
        assert 'success' in rv.data
        self.logout()
        new_perm_id = False
        # + CHANGE USER PASSWORD
        # NO AUTH
        rv = self.app.post('/_set_user_password', data=dict(
            pass_new='test',
            pass_old='admin',
        ), follow_redirects=True)
        assert rv.status_code == 403
        # AUTH
        self.login('admin', 'admin')
        # - NO PARAMS
        rv = self.app.post('/_set_user_password', data=None, follow_redirects=True)
        assert 'Error: Old or new password not defined!' in rv.data
        # - NORMAL
        rv = self.app.post('/_set_user_password', data=dict(
            pass_new='test',
            pass_old='admin',
        ), follow_redirects=True)
        assert 'success' in rv.data
        self.logout()


class LoginSecurityTestCase(unittest.TestCase):

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def setUp(self):
        self.db_fd, self.dbfile = tempfile.mkstemp()
        self.twp_app = create_app(TWPConfigTest())
        self.twp_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///%s' % self.dbfile
        self.twp_app.config['SERVERS_BASEPATH'] = tempfile.mkdtemp()
        self.test_server_folder = os.path.join(self.twp_app.config['SERVERS_BASEPATH'], 'twsrv')
        self.app = self.twp_app.test_client()
        db_init(self.twp_app)
        self.app.post('/_finish_installation', data=dict(
            adminuser='admin',
            adminpass='admin',
            brand='Testing',
        ), follow_redirects=True)

    def tearDown(self):
        BANLIST.clear()
        os.close(self.db_fd)
        os.unlink(self.dbfile)

    def test_login_security(self):
        rv = self.login('1234', '1234')
        assert 'Invalid username or password!' in rv.data
        rv = self.login('12345', '12345')
        assert 'Invalid username or password!' in rv.data
        rv = self.login('123456', '123456')
        assert 'Invalid username or password!' in rv.data
        rv = self.login('1234567', '1234567')
        assert rv.status_code == 403


class RestoreAdminSecurityTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, self.dbfile = tempfile.mkstemp()
        self.twp_app = create_app(TWPConfigTest())
        self.twp_app.config['SECRET_KEY'] = 'abc001'
        self.twp_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///%s' % self.dbfile
        self.twp_app.config['SERVERS_BASEPATH'] = tempfile.mkdtemp()
        self.test_server_folder = os.path.join(self.twp_app.config['SERVERS_BASEPATH'], 'twsrv')
        self.app = self.twp_app.test_client()
        db_init(self.twp_app)
        self.app.post('/_finish_installation', data=dict(
            adminuser='admin',
            adminpass='admin',
            brand='Testing',
        ), follow_redirects=True)

    def tearDown(self):
        BANLIST.clear()
        os.close(self.db_fd)
        os.unlink(self.dbfile)

    def test_login_security(self):
        rv = self.app.post('/restore_admin', data=dict(
            skey='qddsadvvwew',
            username='admin',
            userpass='admin_changed',
            ruserpass='admin_changed',
        ), follow_redirects=True)
        assert 'Invalid Secret!' in rv.data
        rv = self.app.post('/restore_admin', data=dict(
            skey='abc001',
            username='idk',
            userpass='admin_changed',
            ruserpass='admin_changed',
        ), follow_redirects=True)
        assert 'Invalid Admin User Name!' in rv.data
        rv = self.app.post('/restore_admin', data=dict(
            skey='qddsadvvwew',
            username='idk',
            userpass='admin_changed',
            ruserpass='admin_changed',
        ), follow_redirects=True)
        assert 'Invalid Secret!' in rv.data
        rv = self.app.post('/restore_admin', data=dict(
            skey='qddsadvvwew',
            username='idk',
            userpass='admin_changed',
            ruserpass='admin_changed',
        ), follow_redirects=True)
        assert rv.status_code == 403


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TWPTestCase))
    suite.addTest(unittest.makeSuite(LoginSecurityTestCase))
    suite.addTest(unittest.makeSuite(RestoreAdminSecurityTestCase))
    return suite


if __name__ == '__main__':
    unittest.main(defaultTest='suite')
