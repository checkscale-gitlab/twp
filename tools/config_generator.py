# -*- coding: utf-8 -*-
##############################################################################
#    Teeworlds Web Panel
#    Copyright (C) 2016-2017  Alexandre Díaz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################
import os
import re
import json


def create_json_from_project_path(project_path, dest_file, server_side):
    flag = server_side and 'CFGFLAG_SERVER' or 'CFGFLAG_CLIENT'
    config_files = {
        'Game': os.path.join(
            project_path,
            'src',
            'game',
            'variables.h'
        ),
        'Engine': os.path.join(
            project_path,
            'src',
            'engine',
            'shared',
            'config_variables.h'
        )
    }

    config_data = {
        'Game': {},
        'Engine': {},
    }

    for section in config_files.keys():
        with open(config_files[section], 'r') as f:
            lines = f.readlines()
            for line in lines:
                # Try search INT
                results = re.search(
                    'MACRO_CONFIG_INT\([^,]+,\s?([^,]+),\s?(\d+),\s?(\d+),\s?(\d+),\s?([^,]+),\s?\"([^\"]+)\"\)',
                    line
                )
                if results:
                    if flag not in results.group(5):
                        continue
                    is_bool = False
                    if int(results.group(3)) == 0 and int(results.group(4)) == 1:
                        is_bool = True
                    config_data[section].update({
                        results.group(1): {
                            'default': int(results.group(2)),
                            'type': 'checkbox' if is_bool else 'number',
                            'label': ' '.join(results.group(1).split('_')[1:]).title(),
                            'tooltip': results.group(6) or ''
                        }
                    })
                    if not is_bool:
                        config_data[section][results.group(1)].update({
                            'range': [int(results.group(3)), int(results.group(4))]
                        })
                    continue
                # Try search STR
                results = re.search(
                    'MACRO_CONFIG_STR\([^,]+,\s?([^,]+),\s?(\d+),\s?\"([^\"]+)\",\s?([^,]+),\s?\"([^\"]+)\"\)',
                    line
                )
                if results:
                    if flag not in results.group(4):
                        continue
                    config_data[section].update({
                        results.group(1): {
                            'max_length': int(results.group(2)),
                            'default': results.group(3),
                            'type': 'text',
                            'label': ' '.join(results.group(1).split('_')[1:]).title(),
                            'tooltip': results.group(5) or ''
                        }
                    })
    with open(dest_file, 'w') as f:
        json.dump(config_data, f, indent=4)
