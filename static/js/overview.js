"use strict";
/*
 ********************************************************************************************
 **    Teeworlds Web Panel
 **    Copyright (C) 2016-2017  Alexandre Díaz
 **
 **    This program is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU Affero General Public License as
 **    published by the Free Software Foundation, either version 3 of the
 **    License.
 **
 **    This program is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU Affero General Public License for more details.
 **
 **    You should have received a copy of the GNU Affero General Public License
 **    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ********************************************************************************************
 */
//TODO: Change concats to formatted strings
function mastersrv_servers(){
	$.post($SCRIPT_ROOT + '/_get_all_online_servers', '', function(data) {
		check_server_data(data);
		$('#twmslist-load').hide();
		
		if (data['servers'].length)
		{
			var $table = $('#twmslist tbody');
			var cont = 0;
			console.log(data['servers']);
			for (var server of data['servers'])
			{
				var row = "<tr>";
				row += "<td>"+server.name+"</td>";
				row += "<td>"+server.gametype+"</td>";
				row += "<td>"+server.players+"/"+server.max_players+"</td>";
				row += "<td>"+server.map+"</td>";
				//row += "<td class='text-right'>"+Math.round(data['servers'][i].latency*1000)+"</td>";
				row += "</tr>";
				$table.append(row);
			}
			$('#twmslist').show();
		}
		else
		{
			$('#twmslist-empty').show();
		}
	});
}

$(function() {
	$('#twmslist').hide();
	$('#twmslist-empty').hide();
	
	mastersrv_servers();
	
	$.post($SCRIPT_ROOT + '/_get_chart_values/machine', '', function(data) {
		console.log(data);
		// Most played mods in the last 7days
		if (data['series'] && data['series']['mods7d'] && data['series']['mods7d'].length > 0)
			createAnimatedChartBars('#chart-machine-mods7d', data, 'mods7d');
		else
			$('#chart-machine-mods7d').html("<h3 class='text-center text-muted'>"+$BABEL_STR_NO_DATA+"<br/><i class='fa fa-line-chart'></i></h3>");
		// Players last 7days
		if (data['series'] && data['series']['players7d'] && data['series']['players7d'].length > 0)
			createAnimatedChartLineTime('#chart-machine-players7d', data, 'players7d', 6, "MMM DD", true);
		else
			$('#chart-machine-players7d').html("<h3 class='text-center text-muted'>"+$BABEL_STR_NO_DATA+"<br/><i class='fa fa-line-chart'></i></h3>");
		// Players last 24Hours
		if (data['series'] && data['series']['players24h'] && data['series']['players24h'].length > 0)
			createAnimatedChartLineTime('#chart-machine-players24h', data, 'players24h', 24, 'HH:mm', false);
		else
			$('#chart-machine-players24h').html("<h3 class='text-center text-muted'>"+$BABEL_STR_NO_DATA+"<br/><i class='fa fa-line-chart'></i></h3>");
	});
});