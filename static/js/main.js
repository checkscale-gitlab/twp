"use strict";
/*
 ********************************************************************************************
 **    Teeworlds Web Panel
 **    Copyright (C) 2016-2017  Alexandre Díaz
 **
 **    This program is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU Affero General Public License as
 **    published by the Free Software Foundation, either version 3 of the
 **    License.
 **
 **    This program is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU Affero General Public License for more details.
 **
 **    You should have received a copy of the GNU Affero General Public License
 **    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ********************************************************************************************
 */
//TODO: Change concats to formatted strings
$(function(){

	/** MAIN MENU **/
    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));
    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '30%';
    var menuneg = '-100%';
    var slideneg = '-30%';

    $("#slide-nav").on("click", toggler, function(e){

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slideneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');

        //$('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');
    });

    /*var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';
    $(window).on("resize", function(){
        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden'))
            $(selected).removeClass('slide-active');
    });*/
    /** END: MAIN MENU **/

    /** GENERAL **/
	$(document).on("keypress", ".deny-enter :input:not(textarea)", function(event) {
	    if (event.keyCode == 13) {
	        event.preventDefault();
	    }
	});

	refresh_main();
	window.setInterval('refresh_main()', $REFRESH_TIME);

	// Initialize colorpickers
	$('.colorpicker-input').colorpicker();
	/** END: GENERAL **/

	/** TIMEZONE SELECTOR **/
	$(document).on('change', '#tzform #tzstr', function(){
		var $this = $(this);

		$.post($SCRIPT_ROOT + '/_set_timezone', $('#tzform').serialize(), function(data){
			check_server_data(data);

			if (data['success'])
				window.location.reload();
		});
	});

	/** CSS INITIALIZATIONS **/
	$('.carrousel-fade').each(function(){ $(this).carouselanim(); });
	$('.select2').each(function(){ $(this).select2(); });
	var clipboard = new Clipboard('.btn-clipboard');
	clipboard.on('error', function(e) {
		var $trigger = $(e.trigger);
	    alert("Oops! your browser can't copy to clipboard...\nYou need to copy manually:\n\n"+$trigger.data('clipboard-text'));
	});
});

/** AJAX CONFIG **/
var csrftoken = $('meta[name=csrf-token]').attr('content');
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
/** END: AJAX CONFIG **/

/** REST CALLS **/
function refresh_main()
{
	get_server_instances_online();
	get_host_localtime();
}

function get_server_instances_online()
{
    $.post($SCRIPT_ROOT + '/_get_server_instances_online', '', function(data) {
      	 check_server_data(data);

      	 if (data['success'])
      	 {
      		 var num = data['num'];
      		 if (num == 0)
      			 $("#badge-num-server-instances").text("0").addClass('hidden');
      		 else
      			 $("#badge-num-server-instances").text(num).removeClass('hidden');
      	 }
    });
}

function check_server_data(data)
{
	if (!data || data == undefined)
		return;

	if (data['notauth'])
	{
		bootbox.dialog({
			message: "<p class='text-center' style='color:#800000;'><i class='fa fa-warning'></i> "+$BABEL_STR_NO_AUTH+" <i class='fa fa-warning'></i></p>",
			title: $BABEL_STR_TITLE_NO_AUTH,
			buttons: {
				success: {
					label: "Oh!",
					className: "btn-primary",
					callback: function() {
						window.location.reload();
					}
				}
			}
		});
	}
	else if (data['error'])
	{
		var errormsg = data['errormsg']?data['errormsg']:$BABEL_STR_INTERNAL_ERROR;

		bootbox.dialog({
			message: "<p class='text-center' style='color:#800000;'>"+errormsg+"</p>",
			title: $BABEL_STR_ERROR_OOOPS+" <i class='fa fa-frown-o'></i>",
			buttons: {
				success: {
					label: $BABEL_STR_DAMN,
					className: "btn-primary"
				}
			}
		});
	}
}

function get_host_localtime()
{
	$.post($SCRIPT_ROOT + '/_refresh_host_localtime', '', function(data) {
		$("#localtime").text(data['localtime']);
		$("#localzone").text(data['localzone']);
	});
}
/** END: REST CALLS **/


/** CONFIG TOOLS **/
function get_config_value_textarea($ta, param)
{
	var lines = $ta.splitlines();
	for (var i in lines)
	{
        var objMatch = lines[i].match(/^([^#\s]+)\s([^#\r\n]+)/);
        if (objMatch && param.toLowerCase() === objMatch[1].toLowerCase())
        	return objMatch[2];
	}

	return undefined;
}

function update_config_textarea($ta, param, new_value, defval)
{
	var lines = $ta.splitlines();
	var nvalue = "";

	var replaced = false;
	for (var i in lines)
	{
        var objMatch = lines[i].match(/^([^#\s]+)\s([^#\r\n]+)/);
        if (objMatch && param.toLowerCase() === objMatch[1].toLowerCase())
        {
        	if ((typeof defval === 'undefined' && new_value)
        		|| (typeof defval !== 'undefined' && new_value != defval))
        	{
        		nvalue += param+" "+new_value+"\n";
        	}
            replaced = true;
        }
        else
        	nvalue += lines[i]+"\n";
	}

    if (!replaced && ((typeof defval === 'undefined' && new_value)
    					|| (typeof defval !== 'undefined' && new_value != defval)))
    {
    	nvalue += param+" "+new_value+"\n";
    }

    $ta.val(nvalue);
}
/** END: CONFIG TOOLS **/


/** JQUERY EXTENSIONS **/
// Minimal Animated Carrousel
$.fn.extend({
	carouselanim: function(option) {
		var $this = $(this);

		function selItem(index, noAnim)
		{
			var $items = $this.data('carrousel-items');
			var selected = +$this.data('carrousel-sel');

			if (noAnim)
			{
				$items[selected].css('display', 'none');
				$items[index].css({ display:'initial', opacity:1 });
			}
			else
			{
				$items[selected].animate({ opacity:'0' }, function(){
					$items[selected].css('display', 'none');
				});
				$items[index].css({ display:'initial', opacity:0 });
				$items[index].animate({ opacity:1 });
			}
			$this.data('carrousel-sel', index);
		};

		if (typeof(option) === "string")
		{
			switch(option)
			{
			case "next":
				var $items = $this.data('carrousel-items');
				var selected = +$this.data('carrousel-sel');
				if (selected < $items.length-1)
					selItem(selected+1);
				break;
			case "prev":
				var selected = +$this.data('carrousel-sel');
				if (selected > 0)
					selItem(selected-1);
				break;
			}
		}
		else if (typeof(option) === "number")
		{
			selItem(option);
		}
		else
		{
			var $items = [];
			var count=0;
			var activedSel=-1;
			$this.children('.carrousel-fade-list').children('.item').each(function(){
				var $_this = $(this);
				$_this.css({ display:'none', position:'absolute', width: '100%', left: 0 });
				$items.push($_this);
				if ($_this.hasClass('active'))
					activedSel = count;
				++count;
			});
			$this.css({ position:'relative' });
			$this.data('carrousel-items', $items);
			$this.data('carrousel-sel', 0);
			$items[0].css('display', 'initial');

			$this.find('.next').each(function(){
				$(this).click(function(){
					$this.carouselanim('next');
					return false;
				})
			});
			$this.find('.prev').each(function(){
				$(this).click(function(){
					$this.carouselanim('prev');
					return false;
				})
			});

			if (activedSel != -1)
				selItem(activedSel, true);
		}
	}
});

// Split TextArea/Input form controls
$.fn.extend({
	splitlines: function() {
		var lines = this.val().split(/\n/);
		var texts = [];
		for (var i=0; i < lines.length; i++)
		{
			if (/\S/.test(lines[i]))
				texts.push($.trim(lines[i]));
		}

		return texts;
	}
});

/** CHARTS **/
function createAnimatedChartDonut(selector, data, chartType)
{
	var chartData = {'labels':data['labels'][chartType],'series':data['series'][chartType]};
	var chartConfig = {
		donut: true,
		donutWidth: 30,
		labelOffset: 0,
		labelDirection: 'explode'
		//showLabel: false
	};

	var chart = new Chartist.Pie(selector, chartData, chartConfig);

	chart.on('draw', function(data) {
		if(data.type === 'slice') {
			var pathLength = data.element._node.getTotalLength();
			var animationDefinition = {
				'stroke-dashoffset': {
					id: 'anim' + data.index,
					dur: 1000,
					from: -pathLength + 'px',
					to:  '0px',
					easing: Chartist.Svg.Easing.easeOutQuint,
					// We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
					fill: 'freeze'
				}
			};

			data.element.attr({
				'stroke-dashoffset': -pathLength + 'px',
				'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
			});
			if(data.index !== 0)
				animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
			data.element.animate(animationDefinition, false);
		}
	});
}

function createAnimatedChartLine(selector, data, chartType)
{
	var chartData = {'labels':data['labels'][chartType],'series':[data['series'][chartType]]};
	var chartConfig = {
		fullWidth: true,
		axisY: {
			type: Chartist.AutoScaleAxis,
		    onlyInteger: true
		}
	};

	var chart = Chartist.Line(selector, chartData, chartConfig);
	var seq = 0;
	chart.on('created', function() { seq = 0; });
	chart.on('draw', function(data) {
		if(data.type === 'point') {
			data.element.animate({
				opacity: {
					begin: seq++ * 80,
					dur: 500,
					from: 0,
					to: 1
				},
				x1: {
					begin: seq++ * 80,
					dur: 500,
					from: data.x - 100,
					to: data.x,
					easing: Chartist.Svg.Easing.easeOutQuart
				}
			});
		}
	});
	return chart;
}

function createAnimatedChartLineTime(selector, data, chartType, divisor, format, showpoints)
{
	var serie_data = []
	for (var value of data['series'][chartType]) {
		serie_data.push({'x': moment.utc(value['x']).tz($TIMEZONE), 'y': value['y']})
	}

	var chartData = {'series':[{'name': 'series-1', 'data': serie_data}]};
	var chartConfig = {
		showPoint: showpoints,
		fullWidth: true,
		axisX: {
			type: Chartist.FixedScaleAxis,
			divisor: divisor,
			labelInterpolationFnc: function(value) {
				return moment.utc(value).tz($TIMEZONE).format(format); // UTC -> Local
			}
		},
		axisY: {
			type: Chartist.AutoScaleAxis,
		    onlyInteger: true
		}
	};

	return Chartist.Line(selector, chartData, chartConfig);
}

function createAnimatedChartBars(selector, data, chartType)
{
	var chartData = {
		'labels':data['labels'][chartType],
		'series':data['series'][chartType]
	};
	var chartConfig = {
		seriesBarDistance: 10,
		fullWidth: true,
		axisY: {
			type: Chartist.AutoScaleAxis,
		    onlyInteger: true
		}
	};

	return Chartist.Bar(selector, chartData, chartConfig);
}
